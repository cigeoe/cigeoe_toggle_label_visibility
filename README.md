# CIGeoE Identify Dangles

This plugin toggle label visibility.

# Installation

For QGIS 3:

There are two methods to install the plugin:

- Method 1:

  1- Copy the folder “cigeoe_toggle_label_visibility” to folder:

  ```bash
  [drive]:\Users\[user]\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins
  ```

  2 - Start QGis and go to menu "Plugins" and select "Manage and Install Plugins"

  3 - Select “CIGeoE Toggle Label Visibility”

  4 - After confirm the operation the plugin will be available in toolbar

- Method 2:

  1 - Compress the folder “cigeoe_toggle_label_visibility” (.zip).

  2 - Start QGis and go to "Plugins" and select “Manage and Install Plugins”

  3 - Select “Install from Zip”, choose .zip generated in point 1) and then select "Install Plugin"

# Usage

1 - In the “Layers Panel” select the “layer” and put it into editing and activate the labels: 
  - Right-click on the active layer and select “Properties”.
  - Then go to "Layer\Properties\Labels\Single Labels\Label with" and select the attribute with label.

![ALT](/images/image1.png)

2 - Click on plugin icon to activate (all labels visible).

![ALT](/images/image2.png)

3 - Click on plugin icon again to disable plugin.

# Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

# License

[AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)
